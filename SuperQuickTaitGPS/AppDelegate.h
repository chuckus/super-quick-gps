//
//  AppDelegate.h
//  SuperQuickTaitGPS
//
//  Created by Charlie Smith on 25/06/13.
//  Copyright (c) 2013 Charlie Smith. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
